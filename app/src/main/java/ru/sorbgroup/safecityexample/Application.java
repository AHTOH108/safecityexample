package ru.sorbgroup.safecityexample;

import android.content.Context;

import com.nostra13.universalimageloader.cache.memory.impl.UsingFreqLimitedMemoryCache;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;

public class Application extends android.app.Application {

    @Override
    public void onCreate() {
        super.onCreate();

        initImageLoader(this);
    }

    public static void initImageLoader(Context context) {
        ImageLoaderConfiguration.Builder config = new ImageLoaderConfiguration.Builder(context);
        config.threadPoolSize(ImageLoaderConfiguration.Builder.DEFAULT_THREAD_POOL_SIZE - 1);
        config.threadPriority(Thread.NORM_PRIORITY - 2);
        config.denyCacheImageMultipleSizesInMemory();
        int size = Math.round(0.35f * Runtime.getRuntime().maxMemory() / 1024);
        config.memoryCache(new UsingFreqLimitedMemoryCache(size)); // 35% of total
        config.diskCacheSize(50 * 1024 * 1024); // 50 Mb
        config.tasksProcessingOrder(QueueProcessingType.FIFO);

        ImageLoader.getInstance().init(config.build());
    }
}
