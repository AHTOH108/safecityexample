package ru.sorbgroup.safecityexample.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ru.sorbgroup.safecityexample.R;
import ru.sorbgroup.safecityexample.objects.City;
import ru.sorbgroup.safecityexample.utils.MoodUtil;

public class CityDetailActivity extends AppCompatActivity {

    @BindView(R.id.imageView)
    ImageView imageView;
    @BindView(R.id.textViewName)
    TextView textViewName;
    @BindView(R.id.textViewPopulation)
    TextView textViewPopulation;
    @BindView(R.id.imageViewMood)
    ImageView imageViewMood;
    @BindView(R.id.textViewRaiting)
    TextView textViewRaiting;

    private City city = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_city_detail);

        ButterKnife.bind(this);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        city = getIntent().getParcelableExtra("Data");

        showInformation();
    }

    private void showInformation() {
        if (city == null)
            return;
        textViewPopulation.setText(String.format(
                Locale.US,
                "Количество жителей - %s \n\n" +
                        "Из них детей до 18 лет - %s \n\n" +
                        "Протяженность дорог, км - %s\n\n" +
                        "Количество пешеходных переходов - %s \n\n" +
                        "Из них не регулируемых - %s \n\n" +
                        "Количество ДТП - %s \n\n" +
                        "Число лиц погибшив в ДТП - %s \n\n" +
                        "Число ДТП с участием пешеходов - %s \n\n" +
                        "Число детей погибших в ДТП - %s \n\n" +
                        "Социальный риск(число лиц погибших в ДТП на 100 000 населения) - %s \n\n" +
                        "Транспортный риск (число лиц погибших в ДТП на 10 000 транспортных средств) - %s \n\n" +
                        "Количество автомобилей на 1 000 жителей - %s \n\n" +
                        "Количесвто ДТП - %s (База ГБДД от 1 марта 2018 года)\n\n",
                city.getPopulation(),
                city.getPopulationChildren(),
                city.getLengthRoad(),
                city.getCrossWalk(),
                city.getCrossWalkNo(),
                city.getDtp(),
                city.getDtpPeople(),
                city.getDtpCrossWalk(),
                city.getDtpChildren(),
                city.getSocialRisk(),
                city.getTransportRisk(),
                city.getCountAuto(),
                city.getDtpActual()));

        textViewRaiting.setText(String.format(Locale.US, "Рейтинг города: %.2f", city.getReiting()));
        textViewName.setText(city.getName());
        imageView.setImageResource(city.getResIdIcon());
        imageViewMood.setImageResource(MoodUtil.getResId(city.getMood()));

    }

    @OnClick(R.id.button)
    public void onClickButton(){
        startActivity(new Intent(this, QuestionActivity.class));
    }

}
