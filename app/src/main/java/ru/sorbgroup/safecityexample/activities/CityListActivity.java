package ru.sorbgroup.safecityexample.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.sorbgroup.safecityexample.R;
import ru.sorbgroup.safecityexample.objects.City;
import ru.sorbgroup.safecityexample.utils.Constants;

public class CityListActivity extends AppCompatActivity {

    private ArrayList<City> listCity = null;

    private RVCityAdapter adapter;

    @BindView(R.id.radioGroup)
    RadioGroup radioGroup;
    @BindView(R.id.radioButtonName)
    RadioButton radioButtonName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_city_list);

        ButterKnife.bind(this);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        loadCity();

        iniRV();

        radioGroup.setOnCheckedChangeListener((group, checkedId) -> {
            switch (checkedId) {
                case R.id.radioButtonName:
                    sortOnName();
                    break;
                case R.id.radioButtonMood:
                    sortOnMood();
                    break;
                case R.id.radioButtonRaiting:
                    sortOnRaiting();
                    break;
            }
        });

        //radioButtonName.setSelected(true);
        radioButtonName.setChecked(true);
        sortOnName();
    }

    private void sortOnName() {
        if (adapter != null && listCity != null) {
            Collections.sort(listCity, City.CityNameComparator);
            adapter.setListItem(listCity);
        }
    }

    private void sortOnMood() {
        if (adapter != null && listCity != null) {
            Collections.sort(listCity, City.CityMoodComparator);
            adapter.setListItem(listCity);
        }
    }

    private void sortOnRaiting() {
        if (adapter != null && listCity != null) {
            Collections.sort(listCity, City.CityRaitingComparator);
            adapter.setListItem(listCity);
        }
    }

    private void iniRV() {
        adapter = new RVCityAdapter(this, listCity);
        RecyclerView recyclerView = findViewById(R.id.recyclerView);
        GridLayoutManager layoutManager = new GridLayoutManager(this, 2);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(layoutManager);
    }

    private void loadCity() {
        if (listCity == null)
            listCity = new ArrayList<>();
        else
            listCity.clear();

        JSONArray jsonArray;
        try {
            jsonArray = new JSONObject(Constants.listCityJson).getJSONArray("listCity");
            listCity = new Gson().fromJson(jsonArray.toString(), new TypeToken<List<City>>() {
            }.getType());

            iniImage();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void iniImage() {
        if (listCity == null)
            return;

        for (City city : listCity) {
            switch (city.getId()) {
                case 1:
                    city.setResIdIcon(R.drawable.ic_city_moscow);
                    break;
                case 2:
                    city.setResIdIcon(R.drawable.ic_city_ryazan);
                    break;
                case 3:
                    city.setResIdIcon(R.drawable.ic_city_tyla);
                    break;
                case 4:
                    city.setResIdIcon(R.drawable.ic_city_vladimir);
                    break;
                case 5:
                    city.setResIdIcon(R.drawable.ic_city_smolensk);
                    break;
                case 6:
                    city.setResIdIcon(R.drawable.ic_city_kalyga);
                    break;
                case 7:
                    city.setResIdIcon(R.drawable.ic_city_ivanovo);
                    break;
                case 8:
                    city.setResIdIcon(R.drawable.ic_city_tver);
                    break;
                case 9:
                    city.setResIdIcon(R.drawable.ic_city_orel);
                    break;
                case 10:
                    city.setResIdIcon(R.drawable.ic_city_tambov);
                    break;
                default:
                    break;
            }
        }
    }

    private void onClickCity(City city) {

        Intent intent = new Intent(this, CityDetailActivity.class);
        intent.putExtra("Data", city);
        startActivity(intent);
    }

    class RVCityAdapter extends RecyclerView.Adapter<RVCityAdapter.ViewHolder> {

        private LayoutInflater lInflater;
        private ArrayList<City> listItem;
        private Context mContext;

        RVCityAdapter(Context context, ArrayList<City> listItem) {
            this.mContext = context;
            this.lInflater = LayoutInflater.from(context);
            this.listItem = new ArrayList<>();
            this.listItem.addAll(listItem);
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new ViewHolder(lInflater.inflate(R.layout.item_list_city, parent, false));
        }

        @Override
        public void onBindViewHolder(RVCityAdapter.ViewHolder viewHolder, int position) {
            City item = getItem(position);
            viewHolder.view.setTag(item);
            viewHolder.view.setOnClickListener(v -> {
                City item1 = (City) v.getTag();
                if (item1 != null)
                    onClickCity(item1);
            });
            viewHolder.textViewName.setText(String.format("%s (%s)", item.getName(), item.getReiting()));

            if (item.getResIdIcon() > 0)
                viewHolder.imageView.setImageResource(item.getResIdIcon());
            /*int picId = getResources().getIdentifier(item.getResIdIcon(), "drawable", getApplicationContext().getPackageName());
            if (picId > 0)
                viewHolder.imageView.setImageResource(picId);*/
        }

        @Override
        public int getItemCount() {
            return listItem.size();
        }

        @Override
        public int getItemViewType(int position) {
            return 0;
        }

        City getItem(int position) {
            if (listItem.size() > position)
                return listItem.get(position);
            else
                return new City();
        }

        public void setListItem(ArrayList<City> listItem) {
            this.listItem.clear();
            this.listItem.addAll(listItem);
            notifyDataSetChanged();
        }

        class ViewHolder extends RecyclerView.ViewHolder {
            View view;
            TextView textViewName;
            ImageView imageView;

            ViewHolder(View itemView) {
                super(itemView);
                this.view = itemView;
                this.textViewName = view.findViewById(R.id.textViewName);
                this.imageView = view.findViewById(R.id.imageView);

            }
        }
    }

}
