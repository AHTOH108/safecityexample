package ru.sorbgroup.safecityexample.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ru.sorbgroup.safecityexample.R;
import ru.sorbgroup.safecityexample.objects.Question;
import ru.sorbgroup.safecityexample.utils.Generator;

public class QuestionActivity extends AppCompatActivity {

    @BindView(R.id.textViewName)
    TextView textViewName;
    @BindView(R.id.radioGroup)
    RadioGroup radioGroup;
    @BindView(R.id.radioButton1)
    RadioButton radioButton1;
    @BindView(R.id.radioButton2)
    RadioButton radioButton2;
    @BindView(R.id.radioButton3)
    RadioButton radioButton3;
    @BindView(R.id.radioButton4)
    RadioButton radioButton4;
    @BindView(R.id.radioButton5)
    RadioButton radioButton5;
    @BindView(R.id.buttonBack)
    Button buttonBack;
    @BindView(R.id.buttonNext)
    Button buttonNext;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    ArrayList<Question> listQuestion;

    private int position = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question);

        ButterKnife.bind(this);

        listQuestion = new ArrayList<>();
        listQuestion = Generator.getListQuestion();

        progressBar.setMax(listQuestion.size());

        radioButton1.setOnClickListener(v -> listQuestion.get(position).setAnswer(1));
        radioButton2.setOnClickListener(v -> listQuestion.get(position).setAnswer(2));
        radioButton3.setOnClickListener(v -> listQuestion.get(position).setAnswer(3));
        radioButton4.setOnClickListener(v -> listQuestion.get(position).setAnswer(4));
        radioButton5.setOnClickListener(v -> listQuestion.get(position).setAnswer(5));


        /*radioGroup.setOnCheckedChangeListener((group, checkedId) -> {
            int a = -1;
            switch (checkedId) {
                case R.id.radioButton1:
                    a = 1;
                    break;
                case R.id.radioButton2:
                    a = 2;
                    break;
                case R.id.radioButton3:
                    a = 3;
                    break;
                case R.id.radioButton4:
                    a = 4;
                    break;
                case R.id.radioButton5:
                    a = 5;
                    break;
                default:
                    break;
            }
            listQuestion.get(position).setAnswer(a);
            //nextQ();
        });*/

        updateQ();
    }

    @OnClick(R.id.buttonBack)
    public void onClickBack() {
        backQ();
    }

    @OnClick(R.id.buttonNext)
    public void onClickNext() {
        switch (radioGroup.getCheckedRadioButtonId()) {
            case R.id.radioButton1:
                nextQ();
                break;
            case R.id.radioButton2:
                nextQ();
                break;
            case R.id.radioButton3:
                nextQ();
                break;
            case R.id.radioButton4:
                nextQ();
                break;
            case R.id.radioButton5:
                nextQ();
                break;
            default:
                break;
        }
    }

    private void nextQ() {
        if (listQuestion.size() - 1 <= position) {
            finish();
        }
        position++;
        updateQ();
    }

    private void backQ() {
        if (position < 1) {
            return;
        }
        position--;
        updateQ();
    }

    private void updateQ() {
        if (position < 0 || listQuestion.size() <= position) {
            return;
        }

        Question q = listQuestion.get(position);
        textViewName.setText(String.format("%s) %s", q.getId(), q.getName()));
        progressBar.setProgress(position);

        switch (q.getAnswer()) {
            case 1:
                radioButton1.setChecked(true);
                break;
            case 2:
                radioButton2.setChecked(true);
                break;
            case 3:
                radioButton3.setChecked(true);
                break;
            case 4:
                radioButton4.setChecked(true);
                break;
            case 5:
                radioButton5.setChecked(true);
                break;
            default:
                radioButton1.setChecked(false);
                radioButton2.setChecked(false);
                radioButton3.setChecked(false);
                radioButton4.setChecked(false);
                radioButton5.setChecked(false);
                break;
        }

        if (listQuestion.size() - 1 == position)
            buttonNext.setText("Завершить опрос");
        else
            buttonNext.setText("Следующий");

    }
}
