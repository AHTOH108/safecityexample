package ru.sorbgroup.safecityexample.activities;

import android.content.Intent;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import ru.sorbgroup.safecityexample.R;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        if (!getResources().getBoolean(R.bool.is_tablet)) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Внимание!")
                    .setMessage("Приложение не поддерживается на телефонах!")
                    .setCancelable(false)
                    .setPositiveButton("Понятно",
                            (dialog, id) -> closeApp())
                    .setNegativeButton("Заупустить всё равно!",
                            (dialog, id) -> goCityListActivity());
            AlertDialog alert = builder.create();
            alert.show();

            return;
        }

        goCityListActivity();
    }

    private void goCityListActivity() {

        final Class destinationClass = CityListActivity.class;

        new Handler().postDelayed(() -> {

            Intent intent = new Intent(SplashActivity.this, destinationClass);
            startActivity(intent);

            finish();
        }, 1000);
    }

    private void closeApp() {
        this.finish();
    }

}
