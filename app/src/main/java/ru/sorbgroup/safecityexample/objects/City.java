package ru.sorbgroup.safecityexample.objects;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

import java.util.Comparator;

public class City implements Parcelable, Comparable<City> {
    private int id;
    private String name;
    @SerializedName("image_id")
    private int resIdIcon;
    private int population;
    @SerializedName("population_children")
    private int populationChildren;
    @SerializedName("lenght_road")
    private int lengthRoad;
    @SerializedName("cross_walk")
    private int crossWalk; // количество пешеходных переходов
    @SerializedName("cross_walk_no")
    private int crossWalkNo;
    @SerializedName("dtp")
    private int dtp;
    @SerializedName("dtp_people")
    private int dtpPeople;
    @SerializedName("dtp_cross_walk")
    private int dtpCrossWalk;
    @SerializedName("dtp_children")
    private int dtpChildren;
    @SerializedName("social_risk")
    private double socialRisk;
    @SerializedName("transport_risk")
    private double transportRisk;
    @SerializedName("count_auto")
    private int countAuto;
    @SerializedName("dtp_actual")
    private double dtpActual;
    @SerializedName("mood")
    private int mood; // настроение
    @SerializedName("reiting")
    private double reiting;

    @Override
    public int compareTo(@NonNull City compared) {
        int res;

        String name = getName();

        if (name.compareTo(compared.getName()) < 0) {
            res = -1;
        } else {
            res = 1;
        }
        return res;
    }

    public static Comparator<City> CityIdComparator = (item1, item2) -> {

        if (item1.getId() == item2.getId()) {
            return 0;
        }
        if (item1.getId() < item2.getId()) {
            return -1;
        } else {
            return 1;
        }
    };

    public static Comparator<City> CityNameComparator = (item1, item2) -> {
        int res = 0;

        String name = item1.getName();

        if (name.compareTo(item2.getName()) < 0) {
            res = -1;
        } else {
            res = 1;
        }
        return res;
    };

    public static Comparator<City> CityMoodComparator = (item1, item2) -> {
        if (item1.getMood() == item2.getMood()) {
            return 0;
        }
        if (item1.getMood() > item2.getMood()) {
            return -1;
        } else {
            return 1;
        }
    };

    public static Comparator<City> CityRaitingComparator = (item1, item2) -> {
        if (item1.getReiting() == item2.getReiting()) {
            return 0;
        }
        if (item1.getReiting() > item2.getReiting()) {
            return -1;
        } else {
            return 1;
        }
    };

    public City() {
        this.id = -1;
        this.name = "";
        this.resIdIcon = 0;
        this.population = 0;
        this.populationChildren = 0;
        this.lengthRoad = 0;
        this.crossWalk = 0;
        this.crossWalkNo = 0;
        this.dtp = 0;
        this.dtpPeople = 0;
        this.dtpCrossWalk = 0;
        this.dtpChildren = 0;
        this.socialRisk = 0;
        this.transportRisk = 0;
        this.countAuto = 0;
        this.dtpActual = 0;
        this.mood = 0;
        this.reiting = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getResIdIcon() {
        return resIdIcon;
    }

    public void setResIdIcon(int resIdIcon) {
        this.resIdIcon = resIdIcon;
    }

    public int getPopulation() {
        return population;
    }

    public void setPopulation(int population) {
        this.population = population;
    }

    public int getPopulationChildren() {
        return populationChildren;
    }

    public void setPopulationChildren(int populationChildren) {
        this.populationChildren = populationChildren;
    }

    public int getLengthRoad() {
        return lengthRoad;
    }

    public void setLengthRoad(int lengthRoad) {
        this.lengthRoad = lengthRoad;
    }

    public int getCrossWalk() {
        return crossWalk;
    }

    public void setCrossWalk(int crossWalk) {
        this.crossWalk = crossWalk;
    }

    public int getCrossWalkNo() {
        return crossWalkNo;
    }

    public void setCrossWalkNo(int crossWalkNo) {
        this.crossWalkNo = crossWalkNo;
    }

    public int getDtp() {
        return dtp;
    }

    public void setDtp(int dtp) {
        this.dtp = dtp;
    }

    public int getDtpPeople() {
        return dtpPeople;
    }

    public void setDtpPeople(int dtpPeople) {
        this.dtpPeople = dtpPeople;
    }

    public int getDtpCrossWalk() {
        return dtpCrossWalk;
    }

    public void setDtpCrossWalk(int dtpCrossWalk) {
        this.dtpCrossWalk = dtpCrossWalk;
    }

    public int getDtpChildren() {
        return dtpChildren;
    }

    public void setDtpChildren(int dtpChildren) {
        this.dtpChildren = dtpChildren;
    }

    public double getSocialRisk() {
        return socialRisk;
    }

    public void setSocialRisk(double socialRisk) {
        this.socialRisk = socialRisk;
    }

    public double getTransportRisk() {
        return transportRisk;
    }

    public void setTransportRisk(double transportRisk) {
        this.transportRisk = transportRisk;
    }

    public int getCountAuto() {
        return countAuto;
    }

    public void setCountAuto(int countAuto) {
        this.countAuto = countAuto;
    }

    public double getDtpActual() {
        return dtpActual;
    }

    public void setDtpActual(double dtpActual) {
        this.dtpActual = dtpActual;
    }

    public int getMood() {
        return mood;
    }

    public void setMood(int mood) {
        this.mood = mood;
    }

    public double getReiting() {
        return reiting;
    }

    public void setReiting(double reiting) {
        this.reiting = reiting;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.getId());
        parcel.writeString(this.getName());
        parcel.writeInt(this.getResIdIcon());
        parcel.writeInt(this.getPopulation());
        parcel.writeInt(this.getPopulationChildren());
        parcel.writeInt(this.getLengthRoad());
        parcel.writeInt(this.getCrossWalk());
        parcel.writeInt(this.getCrossWalkNo());
        parcel.writeInt(this.getDtp());
        parcel.writeInt(this.getDtpPeople());
        parcel.writeInt(this.getDtpCrossWalk());
        parcel.writeInt(this.getDtpChildren());
        parcel.writeDouble(this.getSocialRisk());
        parcel.writeDouble(this.getTransportRisk());
        parcel.writeInt(this.getCountAuto());
        parcel.writeDouble(this.getDtpActual());
        parcel.writeInt(this.getMood());
        parcel.writeDouble(this.getReiting());
    }

    public static final Parcelable.Creator<City> CREATOR = new Parcelable.Creator<City>() {

        @Override
        public City createFromParcel(Parcel source) {
            return new City(source);
        }

        @Override
        public City[] newArray(int size) {
            return new City[size];
        }
    };

    private City(Parcel source) {
        this.setId(source.readInt());
        this.setName(source.readString());
        this.setResIdIcon(source.readInt());
        this.setPopulation(source.readInt());
        this.setPopulationChildren(source.readInt());
        this.setLengthRoad(source.readInt());
        this.setCrossWalk(source.readInt());
        this.setCrossWalkNo(source.readInt());
        this.setDtp(source.readInt());
        this.setDtpPeople(source.readInt());
        this.setDtpCrossWalk(source.readInt());
        this.setDtpChildren(source.readInt());
        this.setSocialRisk(source.readDouble());
        this.setTransportRisk(source.readDouble());
        this.setCountAuto(source.readInt());
        this.setDtpActual(source.readDouble());
        this.setMood(source.readInt());
        this.setReiting(source.readDouble());
    }
}
