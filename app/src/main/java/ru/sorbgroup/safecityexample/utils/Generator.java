package ru.sorbgroup.safecityexample.utils;

import java.util.ArrayList;

import ru.sorbgroup.safecityexample.objects.Question;

public class Generator {


    public static ArrayList<Question> getListQuestion(){

        ArrayList<Question> listQuestion = new ArrayList<>();

        listQuestion.add(new Question(1,
                "Оцените уровень жизни",
                -1));
        listQuestion.add(new Question(2,
                "Оцените качество дорог",
                -1));
        listQuestion.add(new Question(3,
                "Оцените качество информационного обеспечения на дорогах (реклама, информационное табло и т.д.)",
                -1));
        listQuestion.add(new Question(4,
                "Оцените качество содержания в технически исправном состоянии придорожной сети и объектов регулирования движения (состояние светофоров, дорожных знаков, разметки и т.д.)",
                -1));
        listQuestion.add(new Question(5,
                "Оцените пробки в вашем городе(1- плотный поток, невозможность движения транспортных средств, 5 - свободное движение транспортных средств) ",
                -1));
        listQuestion.add(new Question(6,
                "Оцените быстроту реагирования экстренных служб",
                -1));
        listQuestion.add(new Question(7,
                "Оцените уровень общественного транспорта(техническое состояние общественного транспорта)",
                -1));
        listQuestion.add(new Question(8,
                "Оцените удобство пересечения проезжей части пешеходам",
                -1));
        listQuestion.add(new Question(9,
                "Оцените удобство пересечения проезжей части по надземному переходу",
                -1));
        listQuestion.add(new Question(10,
                "Оцените удобство пересечения проезжей части по подземному переходу",
                -1));
        listQuestion.add(new Question(11,
                "Оцените приспособленность города для лиц с ограниченными возможностями",
                -1));
        listQuestion.add(new Question(12,
                "Оцените состояние остановочных пунктов(наличие скамеек, мусорных урн, навесов, электронного табло с информацией о транспорте)",
                -1));
        listQuestion.add(new Question(13,
                "Оцените культуру вождения общественного транспорта",
                -1));
        listQuestion.add(new Question(14,
                "Оцените качество системы оплаты проезда",
                -1));

        return listQuestion;
    }
}
