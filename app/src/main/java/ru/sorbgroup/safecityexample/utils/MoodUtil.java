package ru.sorbgroup.safecityexample.utils;

import ru.sorbgroup.safecityexample.R;

public class MoodUtil {

    public static int getResId(int id) {
        switch (id) {
            case 1:
                return R.drawable.ic_1;
            case 2:
                return R.drawable.ic_2;
            case 3:
                return R.drawable.ic_3;
            case 4:
                return R.drawable.ic_4;
            case 5:
            default:
                return R.drawable.ic_5;

        }
    }
}
