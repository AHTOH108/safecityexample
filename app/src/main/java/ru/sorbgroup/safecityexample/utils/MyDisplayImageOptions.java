package ru.sorbgroup.safecityexample.utils;

import com.nostra13.universalimageloader.core.DisplayImageOptions;

import ru.sorbgroup.safecityexample.R;

public class MyDisplayImageOptions {

    public static final DisplayImageOptions getOption() {

        return new DisplayImageOptions.Builder().cacheInMemory(true)
                .cacheOnDisc(true).resetViewBeforeLoading(true)
                .showImageForEmptyUri(R.mipmap.ic_launcher)
                .showImageOnFail(R.mipmap.ic_launcher)
                .showImageOnLoading(R.mipmap.ic_launcher).build();
    }
}
